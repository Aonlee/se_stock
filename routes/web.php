<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/admin_check', function () {
    return view('admin_check');
});
Route::get('/admin_pending', function () {
    return view('admin_pending');
});
Route::get('/admin_issue', function () {
    return view('admin_issue');
});
Route::get('/pdf','PDFController@generatePDF');

